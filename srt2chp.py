import lees.File
import re

lines = lees.File.readAllLines(r"D:\Prog\Prog_Python\Subtitles tools\aaa.srt", "cp1251")
for line in lines:
    # Example: 00:01:48,258 --> 00:01:53,757
    matches = re.match(r'^(\d{2}):(\d{2}):(\d{2}),(\d{3}) --> \d{2}:\d{2}:\d{2},\d{3}$', line)
    # print(line,)
    if matches:
        # print(matches.group(1), matches.group(2), matches.group(3), matches.group(4))

        # print(int(matches.group(1)), int(matches.group(2)), int(matches.group(3)))
        second = int(matches.group(1))*60*60 + int(matches.group(2))*60 \
            + int(matches.group(3))

        chapterStr = 'AddChapterBySecond("{},{}","")'.format(second,matches.group(4))
        print(chapterStr)
