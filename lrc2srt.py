import re, datetime
import lees

# [28:21.96]Everybody's always talking.
# [28:24.02]But why?
# ----
# 17
# 00:00:08.480 --> 00:00:10.840
# Everybody's always talking.
#
# 18
# 00:00:10.850 --> 00:00:11.540
# But why?

fName = r'example.lrc'
ss = lees.File.read(fName, 'utf16')
# print(ss)
timingRegExp = r'^\[(\d+:\d+\.\d+)\]'
phrases = re.findall(timingRegExp + r'\s*(.*?)(?=\s*\r?\n' + timingRegExp + '|$)', ss, re.S|re.MULTILINE)

converted = []
for idx, phrase in enumerate(phrases):
    # print(phrase)

    endTime = phrase[2]
    if not endTime: # List phrase
        endTime = phrase[0]
        timeParts = re.findall('(\d+):(\d+)\.(\d+)', endTime)[0]
        second = '{:0>2}'.format(int(timeParts[1]) + 7)
        endTime = '{}:{}.{}'.format(timeParts[0], second, timeParts[2])

    # TODO Fix workaround with {}1 {}0 below when normal time parsing will be implemented
    srtLine = '{}\n00:{}1 --> 00:{}0\n{}\n' \
        .format(idx+1, phrase[0], endTime, phrase[1])
    converted.append(srtLine)
    # print(srtLine)
print('Total phrases:', len(phrases))

# TODO Find a way to add correctly formatted second
srtLine = ('{}\n99:00:00.000 --> 99:00:00.001\n'
    + 'Subtitles version: {:%Y-%m-%d %H:%M}'
    ).format(idx+1, datetime.datetime.now())
converted.append(srtLine)

# TODO targetFileName = lees.Path.SplittedPath(fName)
lees.File.writeAllLines(fName+'.cp1251.srt', converted, 'cp1251')
lees.File.writeAllLines(fName+'.utf8.srt', converted, 'utf8')
